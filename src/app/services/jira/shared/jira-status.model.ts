// Code Style Guide
// https://angular.io/guide/styleguide#general-naming-guidelines

import { JiraIssue } from './jira-issue.model';

export interface JiraStatus {
  name: string;
  color: string;
  orderIndex: number;
  issues?: JiraIssue[];
}
