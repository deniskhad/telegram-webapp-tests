// Code Style Guide
// https://angular.io/guide/styleguide#general-naming-guidelines

export interface JiraIssue {
  summary: string;
  link: string;
}
