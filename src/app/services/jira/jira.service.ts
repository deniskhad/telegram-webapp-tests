import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JiraStatus } from './shared/jira-status.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JiraService {
  constructor(private http: HttpClient) {
  }

  getIssues(): Observable<JiraStatus[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
      })
    };

    return this.http.get<JiraStatus[]>(environment.jiraProxyApiUrl, httpOptions);
  }
}
