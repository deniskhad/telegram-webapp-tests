import { Component, inject, OnInit } from '@angular/core';
import { JiraService } from './services/jira/jira.service';
import { JiraIssue } from './services/jira/shared/jira-issue.model';
import { JiraStatus } from './services/jira/shared/jira-status.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'jira-api';
  jiraStatuses: JiraStatus[] = [];

  constructor(private jiraService: JiraService) { }

  ngOnInit(): void {
    this.getJiraIssues();
  }

  getJiraIssues(): void {
    this.jiraService.getIssues()
      .subscribe(jiraStatuses => this.jiraStatuses = jiraStatuses);
  }
}
